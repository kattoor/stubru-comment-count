var http = require('http')
var fs = require('fs')

const server = http.createServer((req, res) => {

    switch (req.url) {

        case '/':

            const data = fs.readFileSync('../log-1500578960102', 'utf8')
                .split('\n')
                .slice(0, -1)
                .map(line => line.split(':'))

            const chartLabels = data.map(line => {
                const date = new Date(+line[1])
                const hours = date.getHours()
                const minutes = date.getMinutes() < 10 ? '0'+ date.getMinutes() : date.getMinutes()
                return `'${hours}:${minutes}'`
            }).join(',')

            const chartData = data.map(line => line[0])
            const chartDataDelta = chartData.map((amountOfComments, index) => index === 0 ? 0 : amountOfComments - chartData[index - 1]).join(',')


            const html = fs.readFileSync('../frontend/index.html', 'utf8')
                .replace("'{{data}}'", chartDataDelta)
                .replace("'{{labels}}'", chartLabels)

            res.end(html)
            break

        case '/bower_components/chart.js/dist/Chart.js':
            res.end(fs.readFileSync('../' + req.url, 'utf8'))
            break

        default:
            res.end('200')
            break
    }
})

server.listen(3000, 'localhost')