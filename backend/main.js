var request = require('request')
var fs = require('fs')

const fileName = 'log-' + new Date().getTime()

for (let i = 0; i < 10000; i++) {
    ((i) => {
        setTimeout(() => {
            request({'url': 'https://graph.facebook.com/v2.10/10156325749478056/comments?summary=true&access_token=:)'}, (error, response, body) => {
                if (!error && response.statusCode === 200)
                    fs.appendFileSync(
                        fileName,
                        `${JSON.parse(body).summary.total_count}:${new Date().getTime()}\n`,
                        'utf8')
                else
                    console.log(error + ' - ' + response)
            })
        }, i * 60000)
    })(i)
}
